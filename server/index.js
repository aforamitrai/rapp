import express from "express";
import Mongoose from "mongoose";
import cors from "cors";

const app = express();
app.use(cors());
app.use(express.json());
Mongoose.connect("mongodb://localhost:27017/slots", { useNewUrlParser: true, useUnifiedTopology: true }, (err, res) => {
    if (err) {
        throw new Error(err);
    } else {
        console.log("Connected to Database ");
    }
});

const slotSchema = new Mongoose.Schema({
    time: {
        type: Date,
        require: true
    },
    firstName: {
        type: String,
        require: true,

    },
    lastName: {
        type: String,
        require: true
    },
    phone: {
        type: Number,
        require: true
    },
    timeSlot: {
        type: String,
        unique: true
    },
    booked: Boolean

});

const slotMode = Mongoose.model("slot", slotSchema);



app.get("/", async (req, res) => {
    const allSlot = await slotMode.find();
    console.log(allSlot);
    return res.status(200).json(allSlot);
});

app.post("/add", async (req, res) => {
    const { body } = req;
    console.log(body, "body from fn");

    const createdSlot = await slotMode.create(body);
    return res.status(200).json({ data: createdSlot });

});

app.get("/slot/:id", async (req, res) => {

    const { id } = req.params;
    try {
        const slot = await slotMode.findById(id);
        console.log(slot);
        return res.status(200).json({ data: slot });
    } catch (error) {
        throw new Error(error);
    }
});

app.put("/slot/:id", async (req, res) => {
    ;
    const { id } = req.params;
    try {
        const data = await slotMode.findByIdAndUpdate(id, req.body);
        return res.status(200).json({ data: data });
    } catch (error) {
        throw new Error(error);
    }
});


app.listen(8080, () => console.log("on http://localhost:8080"));