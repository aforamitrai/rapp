import { Provider } from 'react-redux';
import Slots from './components/Slots';
import React from 'react';
import store from './store';
import AddSlot from './components/AddSlot';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className='bg-gray-300 w-screen h-screen'>
          <div className='w-10/12 h-screen bg-gray-100  mx-auto   text-black'>
            <div className='flex justify-center'>
              <Switch>
                <Route path='/' exact component={Slots} />
                <Route path='/add' exact component={AddSlot} />
                <Route path='/add/:id' exact component={AddSlot} />
              </Switch>
            </div>
          </div>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
