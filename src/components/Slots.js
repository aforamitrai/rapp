import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { fetchSlots } from "../reducers/actions/appActions";

function Slots(props) {
    console.log(props);
    useEffect(() => {
        props.fetchSlots();
    }, []);
    return (

        <div className="flex flex-col   w-full px-10 py-10 rounded-lg">
            <div>

                <Link to="/add" >
                    <button className="border border-blue-100 px-10 py-2 rounded-sm bg-blue-400 text-white font-bold uppercase hover:bg-blue-500">Add Slot</button>
                </Link>
            </div>
            <div className="my-10">

                {props.slots ?
                    props.slots.map(slot => {
                        return (
                            <Link to={`/add/${slot._id}`} key={slot._id}>
                                <div className="bg-green-400 py-3 px-10 my-2 text-white font-bold rounded-md flex" >
                                    <div>
                                        {slot.timeSlot}
                                    </div>
                                </div>
                            </Link>
                        );
                    })
                    : null}

            </div>


        </div>
    );
}

function mapStateToProps(state) {
    const { slots } = state;
    return slots;

}

export default connect(mapStateToProps, { fetchSlots })(Slots);