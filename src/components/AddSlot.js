import React, { useEffect } from "react";
import { connect } from "react-redux";
import { addSlot, getSlot, updateSlot } from "../reducers/actions/appActions";



const AddSlot = (props) => {

    // console.log(props.slots);
    const [firstName, setFirstName] = React.useState("");
    const [lastName, setLastName] = React.useState("");
    const [phone, setPh] = React.useState("");
    const [timeSlot, setTimeSlot] = React.useState("");
    const [available, setAvailble] = React.useState([]);
    const [unAvailable, setUnAvailable] = React.useState([]);
    const allSlots = ["9:00", "10:00", "11:00", "12:00", "1:00", "2:00", "3:00", "4:00", "5:00"];
    const { id } = props.match.params;
    const handleSubmit = () => {
        // console.log(timeSlot);
        if (props.match.params.id) {

            props.updateSlot(id, { firstName, lastName, phone, timeSlot }, props.history);
            console.log("object");
        } else {
            props.addSlot({ firstName, lastName, phone, timeSlot }, props.history);
            console.log("add slot");
        }
    };

    const handleCancel = () => {
        props.history.push("/");
    };




    useEffect(() => {
        // if (Object.keys(props.slots.slot).length === 0) {
        console.log(id, "id changed");
        if (id) {

            props.getSlot(`${props.match.params.id}`);
        }
        //     console.log("request");
        // } else {
        //     console.log("EROROR");
        //     const { firstName, lastName, phone, timeSlot } = props.slots.slot;
        //     console.log(firstName);
        //     setFirstName(firstName);
        //     setLastName(lastName);
        //     setPh(phone);
        //     setTimeSlot(timeSlot);

        // }

        props.slots.slots.filter((slot) => {
            if (slot._id === props.match.params.id) {
                setFirstName(slot.firstName);
                setLastName(slot.lastName);
                setPh(slot.phone);
                setTimeSlot(slot.timeSlot);
            }

        });

        // props.slots.slots.map((slot) => {
        //     console.log(allSlots.includes(slot.timeSlot));
        //     if (allSlots.includes(slot.timeSlot)) {
        //         setUnAvailable(state => [...state, slot.timeSlot]);
        //         // console.log(available, "data");
        //         console.log(slot, "includes");
        //         return null;
        //     }
        //     if (!allSlots.includes(slot.timeSlot)) {
        //         setAvailble(slot.timeSlot);
        //         console.log(slot, "no include");
        //         return null;
        //     }
        // });


    }, [id]);

    return (
        <div className="space-y-2 my-10">

            <div className="flex flex-col">
                <label htmlFor="firstName">First Name</label>
                <input type="text" required className="py-2 px-2" placeholder="First Name" onChange={(event) => setFirstName(event.target.value)} value={firstName} />
            </div>
            <div className="flex flex-col">
                <label htmlFor="lastName">Last Name</label>
                <input type="text" required className="py-2 px-2" placeholder="Last Name" onChange={(event) => setLastName(event.target.value)} value={lastName} />
            </div>
            <div className="flex flex-col">
                <label htmlFor="lastName">Phone No</label>
                <input type="tel" required className="py-2 px-2" placeholder="+0122-32-54" onChange={(event) => setPh(event.target.value)} value={phone} />
            </div>

            <div className="flex flex-col" >
                <label htmlFor="slot11">Time Slot</label>
                <select className="px-2 py-2" required onChange={(e) => setTimeSlot(e.target.value)} value={timeSlot} >


                    <option value="" >Select Time slot</option>
                    <option value="9:00">9:00 AM</option>
                    <option value="10:00">10:00 AM</option>
                    <option value="11:00">11:00 AM</option>
                    <option value="12:00">12:00 AM</option>
                    <option value="1:00">1:00 PM</option>
                    <option value="2:00">2:00 PM</option>
                    <option value="3:00">3:00 PM</option>
                    <option value="4:00">4:00 PM</option>
                    <option value="5:00">5:00 PM</option>
                    {/* {
                        unAvailable.map((aval) => {
                            console.log(eval, "asas");
                            return <option value={aval} key={aval}>{aval}</option>;
                        })
                    } */}


                </select>
            </div>
            <button className="bg-blue-400 hover:bg-blue-500 py-2 w-full text-white font-bold uppercase" onClick={handleSubmit}>Submit</button>
            {unAvailable}
            {props.match.params.id ?

                <div className="space-y-2" >

                    <button className="bg-red-400 hover:bg-red-500 py-2 w-full text-white font-bold uppercase" onClick={handleCancel} >Cancel </button>
                    <button className="bg-yellow-400 hover:bg-yellow-500 py-2 w-full text-white font-bold uppercase" >Clear Slot </button>
                </div>
                : null
            }
        </div>
    );
};

function mapStateToProps(state) {

    return state;
}

export default connect(mapStateToProps, { addSlot: addSlot, getSlot: getSlot, updateSlot: updateSlot })(AddSlot);