import { GET_SLOTS, ADD_SLOT, CURRENT_SLOT } from "./types";

import request from "../../uitls/request";
export const fetchSlots = () => async dispatch => {
    try {
        const response = await request.get("/");
        console.log(response.data, "AAA");
        dispatch({ type: GET_SLOTS, payload: response.data });
    } catch (error) {
        throw new Error(error);
    }
};

export const addSlot = (slotData, history) => async dispatch => {
    try {

        const data = await request.post("/add", slotData);
        console.log(data);
        history.push("/");

    } catch (error) {
        throw new Error(error);
    }

    // dispatch({type:ADD_SLOT,payload:})
};

export const getSlot = (slotId) => async dispatch => {
    try {
        const response = await request.get(`/slot/${slotId}`,);
        dispatch({ type: CURRENT_SLOT, payload: response.data });
    } catch (error) {
        throw new Error(error);
    }
};

export const updateSlot = (slotId, data, history) => async dispatch => {
    try {
        const response = await request.put(`/slot/${slotId}`, data);
        dispatch({ type: CURRENT_SLOT, payload: response.data });
        history.push("/");

    } catch (error) {
        throw new Error(error);
    }
};