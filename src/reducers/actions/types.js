export const GET_SLOTS = "GET_SLOTS";
export const ADD_SLOT = "ADD_SLOT";
export const CURRENT_SLOT = "CURRENT_SLOT";
export const CLEAR_SLOT = "CLEAR_SLOT";
