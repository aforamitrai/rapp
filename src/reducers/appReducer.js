import { GET_SLOTS, CURRENT_SLOT, CLEAR_SLOT } from "./actions/types";

const initialState = {
    slots: [],
    slot: {}
};

export default function appReducer(state = initialState, action) {
    switch (action.type) {
        case GET_SLOTS:
            return { ...state, slots: action.payload };
        case CURRENT_SLOT:
            return { ...state, slot: action.payload.data };
        case CLEAR_SLOT:
            return { ...state, slot: null };

        default:
            return state;
    }

}